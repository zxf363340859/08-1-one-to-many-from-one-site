package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ParentEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_and_get_parent_and_child_entity() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们已经分别进行了持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且进行持久化。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start--
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
            closureValue.setValue(savedParentEntity.getId());
        });

        flushAndClear(em -> {
            parentEntity.addChildren(childEntity);
            parentEntityRepository.save(parentEntity);
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValue.getValue());
            assertEquals(1, parentEntityOptional.get().getChildren().size());
        });

        // --end-->
    }

    @Test
    void should_save_parent_and_child_at_once() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们均未进行持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且直接对 parent 进行持久化时。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start-\
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValue = new ClosureValue<>();

        flushAndClear(em -> {
            parentEntity.addChildren(childEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            closureValue.setValue(savedParentEntity.getId());
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValue.getValue());
            assertEquals(1, parentEntityOptional.get().getChildren().size());
        });
        //});
    }

    @Test
    void should_remove_child() {
        // TODO
        //
        // 请书写如下测试：
        //
        // Given 持久化的 parent 和 child。并且 parent 包含 child。
        // When 当使用某种方法从 parent 中移除 child 并进行持久化。
        // Then 再次查询 parent 的时候，parent 的 children 中已经不包含 child，即关联关系取消了。但是
        //   child 记录本身并未被删除。
        //
        // 在写出测试的过程中也需要考虑如何才能够正确的映射 Entity 呢？
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForParentEntity = new ClosureValue<>();
        ClosureValue<Long> closureValueForChildEntity = new ClosureValue<>();
        flushAndClear(em -> {
            parentEntity.addChildren(childEntity);
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            closureValueForParentEntity.setValue(parentEntity.getId());
            ChildEntity savedChildEntity = parentEntity.getChildren().get(0);
            closureValueForChildEntity.setValue(savedChildEntity.getId());
        });

        flushAndClear(em -> {
            parentEntity.deleteChildren(childEntity);
            parentEntityRepository.save(parentEntity);
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            assertEquals(0, parentEntityOptional.get().getChildren().size());
        });

        run(em -> {
            Optional<ChildEntity> childEntityOptional = childEntityRepository.findById(closureValueForChildEntity.getValue());
            assertNotNull(childEntityOptional.get());
        });

        // --end->
    }
}
